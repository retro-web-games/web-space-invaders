import Core from './Core.js'

import Playground from './Objects/Playground'
import Player from './Objects/Player'
import AlienManager from './Objects/AlienManager'
import Shield from './Objects/Shield'

class Game extends Core {
    constructor(container, opt){
        if(opt == null) opt = {}
        opt.max_fps = 60
        super(container, opt)
        this.container.nb_shields = 4
        this.container.shots = []
        this.container.addShot = (shot) => {
            shot.index = this.container.shots.push(shot)-1
        }
        this.container.actions = {
            collisions: {
                Alien_Shot: (alien, shot) => {
                    this.objects.alienManager.kill(alien)
                    this.objects.player.score += alien.score
                }
            }
        }

        this.objects = {}
        this.objects.playground = new Playground({}, this.container)
        this.objects.player = new Player({}, this.container)
        this.objects.alienManager = new AlienManager({}, this.container)
        this.objects.shots = this.container.shots
        this.objects.shields = []
        
        let shield_gap =  Math.round(this.container.canvas.width/(this.container.nb_shields*2+1))
        for(let index = 0 ; index < this.container.nb_shields ; index++){
            this.objects.shields.push(new Shield({ x: shield_gap+index*shield_gap*2 }, this.container))
        }

        document.addEventListener('keydown', function(event){
            this.objects.player.handleDown(event.keyCode)
        }.bind(this))

        document.addEventListener('keyup', function(event){
            this.objects.player.handleUp(event.keyCode)
        }.bind(this))

        this.death = 0

        this.init()
        this.run()
    }

    update(){
        if(this.objects.alienManager.aliens == 0){
            this.reset()
        }
        if(this.objects.player.shield.remaining < 0){
            this.init()
        }

        this.draw()

        let collision_items = [].concat(
            ...this.objects.alienManager.objects, 
            this.objects.player, 
            this.objects.shots,
            this.objects.shields)
        let already_collisioned = []
        collision_items.forEach((item, i) => {
            collision_items.forEach((collision_item, j) => {
                if(
                    (
                        already_collisioned[j] == null || 
                        !already_collisioned[j].includes(i)
                    ) 
                    &&
                    (
                        already_collisioned[i] == null || 
                        !already_collisioned[i].includes(j)
                    )
                ){
                    this.collision(item, collision_item)

                    already_collisioned[i] = already_collisioned[i] || []
                    already_collisioned[i].push(j)
                    already_collisioned[j] = already_collisioned[j] || []
                    already_collisioned[j].push(i)
                }
            })
        })

        this.container.context.fillText('SCORE ' + this.objects.player.score, 20, 60)
        this.container.context.fillText('SHIELD ' + this.objects.player.shield.remaining, 20, 80)
    }

    collision(item1, item2){
        if(
            item1 != null && 
            item2 != null && 
            item1 != item2 && 
            item1.collisions &&
            item2.collisions &&
            item1.collision.left(item2) < item2.collision.right(item1) &&
            item1.collision.right(item2) > item2.collision.left(item1) &&
            item1.collision.top(item2) < item2.collision.bottom(item1) &&
            item1.collision.bottom(item2) > item2.collision.top(item1)
        ){
            item1.impact(item2)
            item2.impact(item1)

            let label1 = item1.constructor.name + '_' + item2.constructor.name
            let label2 = item2.constructor.name + '_' + item1.constructor.name

            let collision_actions = this.container.actions.collisions

            if(collision_actions[label1]){
                collision_actions[label1](item1, item2)
            }
            else if(collision_actions[label2]){
                collision_actions[label2](item2, item1)
            }
        }
    }

    init(){
        this.objects.player.init()
        this.reset()
    }

    reset(){
        this.container.shots.length = 0
        this.objects.alienManager.init()
    }
}

export default Game