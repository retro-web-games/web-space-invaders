import Moveable from './Moveable'

export default class Shot extends Moveable {
    constructor(data, container){
        super(data, container)

        this.velocity.x = 0
        this.width = 2
        this.height = 10
    }

    draw(){
        this.x += this.velocity.x
        this.y += this.velocity.y

        if(this.collision.left() < 0 || this.collision.right() > this.container.canvas.width || this.collision.top() < 0 || this.collision.bottom() > this.container.canvas.height){
            this.impact()
        }

        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x, this.y, this.width, this.height)
    }

    impact(){
        this.container.context.fillStyle = 'white'
        this.container.context.beginPath();
        this.container.context.arc(this.x+this.width/2, this.y+this.height/2, this.height*2, 0, 2 * Math.PI, false);
        this.container.context.fill();

        delete this.container.shots[this.index]
    }
}