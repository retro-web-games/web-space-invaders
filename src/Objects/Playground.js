import Drawable from './Drawable'

class Playground extends Drawable {
    constructor(data, container){
        super(data, container)

        let offset = 2
        let size = 10
        let lines = Math.floor(this.container.canvas.height/(offset+size))
        let columns = Math.floor(this.container.canvas.width/(offset+size))

        this.container.grid = {
            lines,
            columns,
            offset,
            size,
            width: columns*(offset+size),
            height: lines*(offset+size)
        }
    }

    draw(){
        this.container.context.fillStyle = 'black'
        this.container.context.fillRect(0, 0, this.container.grid.width, this.container.grid.height)
    }
}

export default Playground