import Moveable from './Moveable'
import Shot from './Shot'

export default class Player extends Moveable {
    constructor(data, container){
        super(data, container)

        this.initial_shield = 5
        this.shield = {}
        this.width = 80
        this.height = 20
        this.speed = 10

        this.shoot_available = true
        this.reload_time = 1000
        this.shoot_reloading = this.reload_time

        this.shield_collision = {
            top: item => {
                let item_half = (item.collision.right(this)-item.collision.left(this))/2
                let center_offset = item.collision.left(this) - (this.shield.x)
                return this.shield.y - Math.sqrt(Math.pow(this.shield.radius, 2)-Math.pow(center_offset, 2))
            },
            bottom: item => {
                let item_half = (item.collision.right(this)-item.collision.left(this))/2
                let center_offset = item.collision.left(this) - (this.shield.x)
                return this.shield.y + Math.sqrt(Math.pow(this.shield.radius, 2)-Math.pow(center_offset, 2))
            },
            right: item => {
                let item_half = (item.collision.bottom(this)-item.collision.top(this))/2
                let center_offset = this.shield.y - item.collision.bottom(this)
                return this.shield.x + Math.sqrt(Math.pow(this.shield.radius, 2)-Math.pow(center_offset, 2))
            },
            left: item => {
                let item_half = (item.collision.bottom(this)-item.collision.top(this))/2
                let center_offset = this.shield.y - item.collision.bottom(this)
                return this.shield.x - Math.sqrt(Math.pow(this.shield.radius, 2)-Math.pow(center_offset, 2))
            }
        }

        this.no_shield_collision = {
            top: this.collision.top,
            bottom: this.collision.bottom,
            left: this.collision.left,
            right: this.collision.right
        }
    }

    draw(){
        if(this.shield.remaining > 0){
            this.collision = this.shield_collision
        }
        else{
            this.collision = this.no_shield_collision
        }

        this.x += this.velocity.x
        this.y += this.velocity.y

        this.shield.radius = this.width/2+this.height+this.shield.remaining
        this.shield.x = this.x+this.width/2
        this.shield.y = this.y+this.height/2

        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x, this.y, this.width, this.height)

        this.container.context.beginPath();
        this.container.context.arc(this.shield.x, this.shield.y, this.shield.radius, 0, 2 * Math.PI, false);
        this.container.context.fillStyle = 'transparent';
        this.container.context.fill();
        if(this.shield.remaining > 0){
            this.container.context.lineWidth = this.shield.remaining*2;
            this.container.context.strokeStyle = 'white';
            this.container.context.stroke();
        }

        if(!this.shoot_available){
            this.shoot_reloading -= Math.round(1000/this.container.fps)
        }

        if(this.shoot_reloading < 0){
            this.shoot_available = true
            this.shoot_reloading = this.reload_time
        }

        if(this.shooting && this.shoot_available){
            let shot = new Shot({ velocity: { y: -10 } }, this.container)
            shot.x = Math.round(this.x + this.width/2 - shot.width/2)
            shot.y = Math.round(this.y - shot.height)
            this.container.addShot(shot)
            this.shoot_available = false
        }
    }

    impact(){
        this.shield.remaining--
    }

    handleDown(key){
        if(key == LEFT_ARROW){
            this.velocity.x = -this.speed
        }
        if(key == RIGHT_ARROW){ 
            this.velocity.x = this.speed
        }
        if(key == SPACE_BAR){
            this.shooting = true
        }
    }

    handleUp(key){
        if(key == LEFT_ARROW && this.velocity.x == -this.speed){
            this.velocity.x = 0
        }
        if(key == RIGHT_ARROW && this.velocity.x == this.speed){ 
            this.velocity.x = 0
        }
        if(key == SPACE_BAR){
            this.shooting = false
        }
    }

    init(){
        this.shield.remaining = this.initial_shield
        this.score = 0

        this.x = this.container.canvas.width/2-this.width/2
        this.y = this.container.canvas.height-this.height
    }
}