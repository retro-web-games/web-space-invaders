import Moveable from './Moveable'
import Alien from './Alien'
import Shot from './Shot'

export default class AlienManager extends Moveable {
    constructor(data, container){
        super(data, container)

        this.columns = 11
        this.lines = 5
        this.gap = 4

        this.max_aliens = this.columns*this.lines

        this.gutters = Math.floor(this.container.canvas.width/100)
        this.moving_space = 1

        this.drawing_move_max = Math.floor(this.container.canvas.width/((this.columns+(this.gap*2))*5))
        this.drawing_move_min = Math.floor(this.container.canvas.width/2)
        this.drawing_move = this.drawing_move_max
        this.drawing_moving = 0
        this.drawing_moved = 0

        this.shoot_rate_max = 1000
        this.shoot_rate_min = 500
        this.shoot_rate = this.shoot_rate_max
    }

    draw(){
        let frames = 1/this.container.fps

        this.drawing_moving += this.drawing_move*frames
        let move = Math.floor(this.drawing_moving)
        this.velocity.x = this.velocity_tmp.x * move
        this.velocity.y = this.velocity_tmp.y * move

        this.x += this.velocity.x
        this.y += this.velocity.y

        let x = this.x
        let y = this.y

        let column_min = this.columns
        let column_max = 0
        let current_columns = 0

        for(let column = 0 ; column < this.columns ; column++){
            let tmp = 0
            for(let line = 0 ; line < this.objects.length ; line++){
                if(this.objects[line][column].collisions){
                    if(column_min > column){ column_min = column }
                    if(column_max < column){ column_max = column }
                    current_columns++
                    break;
                }
            }
        }

        for(let i = 0 ; i < this.objects.length ; i++){
            for(let j = 0 ; j < this.objects[i].length ; j++){
                this.objects[i][j].x = x
                this.objects[i][j].y = y
                if(this.objects[i][j].collisions){
                    this.objects[i][j].draw()
                }
                x += this.alien_size+this.gutters
            }
            x = this.x
            y += this.alien_size+this.gutters
        }

        this.drawing_move = this.drawing_move_max + (this.drawing_move_min-this.drawing_move_max) * Math.pow(1-this.aliens/this.max_aliens, 10)

        let offset_max_x = this.x+((this.alien_size+this.gutters)*(column_max+1))
        let offset_min_x = this.x+((this.alien_size+this.gutters)*column_min)

        if(offset_max_x > this.max_x){
            this.drawing_moved+=10
            this.velocity_tmp.x = -1
            this.x = this.max_x-((this.alien_size+this.gutters)*(column_max+1))
        }

        if(offset_min_x < this.min_x){
            this.drawing_moved+=10
            this.velocity_tmp.x = 1
            this.x = this.min_x-((this.alien_size+this.gutters)*column_min)+1
        }

        if(offset_max_x > this.max_x-this.alien_size-this.gutters){
            this.velocity_tmp.y = Math.pow(1-((this.max_x-offset_max_x)/(this.alien_size+this.gutters) || 0), 2)
        }
        else if(offset_min_x < this.min_x+this.alien_size-this.gutters){
            this.velocity_tmp.y = Math.pow(1-((offset_min_x-this.min_x)/(this.alien_size+this.gutters) || 0), 2)
        }
        else{
            this.velocity_tmp.y = 0
        }

        if(move > 0){
            this.drawing_moving -= Math.floor(this.drawing_moving)
        }

        this.shootRng()
    }

    shootRng(){
        if(this.shoot_rate > 0){
            this.shoot_rate -= Math.round(1000/this.container.fps)
        }

        if(this.shoot_rate <= 0){
            this.shoot_rate = Math.round(Math.random()*(this.shoot_rate_max-this.shoot_rate_min))+this.shoot_rate_min
            this.shoot()
        }
    }

    shoot(){
        let columnLastIndex = []
        for(let column = 0 ; column < this.columns ; column++){
            for(let line = 0 ; line < this.objects.length ; line++){
                if(this.objects[line][column].collisions){
                    if(columnLastIndex[column] == null){ columnLastIndex[column] = line }
                    if(columnLastIndex[column] < line){ columnLastIndex[column] = line }
                }
            }
        }

        let founded = false
        let shooter = null;
        if(columnLastIndex.length > 0){
            while(shooter == null){
                let rngColumn = Math.round(Math.random()*(this.columns-1))
                if(columnLastIndex[rngColumn] != null){
                    shooter = { column: rngColumn, line: columnLastIndex[rngColumn] }
                }
            }

            let shot_x = this.x + ((this.alien_size+this.gutters)*shooter.column) + ((this.alien_size+this.gutters)/2)
            let shot_y = this.y + ((this.alien_size+this.gutters)*(shooter.line+1))

            let shot = new Shot({ velocity:{ y: 10 } }, this.container)
            shot.x = shot_x - shot.width/2
            shot.y = shot_y
            this.container.addShot(shot)
        }
    }

    kill(alien){
        let target = this.objects[alien.index.y][alien.index.x]
        if(target.collisions){
            this.aliens--
            target.collisions = false
        }
    }

    init(){
        this.drawing_move = this.drawing_move_max
        this.drawing_moved = 0

        this.current_columns = this.columns
        this.current_lines = this.lines

        this.aliens = this.max_aliens
        this.alien_size = Math.floor((this.container.canvas.width-(this.gutters*2)-((this.columns+this.gap)*this.gutters))/(this.columns+this.gap))

        this.x = this.gutters+(this.alien_size+this.gutters)*(this.gap/2)
        this.y = this.gutters
        this.max_x = this.container.canvas.width - this.gutters
        this.min_x = this.gutters

        this.velocity_tmp = { x: 1, y: 0 }

        this.objects = []
        for(let i = 0 ; i < this.lines ; i++){
            this.objects[i] = this.objects[i] || []
            for(let j = 0 ; j < this.columns ; j++){
                this.objects[i][j] = new Alien({ width: this.alien_size }, this.container)
                this.objects[i][j].index = { x: j, y: i }
                this.objects[i][j].score = i*10
            }
        }
    }
}