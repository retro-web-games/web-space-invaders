import Drawable from './Drawable'

export default class Alien extends Drawable {
    constructor(data, container){
        super(data, container)

        this.height = this.width
        this.alive = true
    }

    draw(){
        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x, this.y, this.width, this.height)
    }
}