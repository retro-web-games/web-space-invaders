class Drawable{
    constructor(data, container){
        this.container = container

        this.x = data.x
        this.y = data.y
        this.width = data.width
        this.height = data.height

        this.collisions = true
        this.collision = {
            left: item => this.x,
            top: item => this.y,
            right: item => this.x+this.width,
            bottom: item => this.y+this.height
        }
    }

    draw(){
        console.log('draw function - todo')
    }

    impact(){}
}

export default Drawable;