import Drawable from './Drawable'

class Moveable extends Drawable {
    constructor(data, container){
        super(data, container)

        this.velocity = data.velocity || { x: 0, y: 0}
    }
}

export default Moveable;