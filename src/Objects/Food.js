import Drawable from './Drawable'

class Food extends Drawable {
    constructor(data, container){
        super(data, container)

        this.x = Math.floor(Math.random()*this.container.grid.columns)
        this.y = Math.floor(Math.random()*this.container.grid.lines)
    }

    draw(){
        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x*(this.container.grid.size+this.container.grid.offset), this.y*(this.container.grid.size+this.container.grid.offset), this.container.grid.size, this.container.grid.size)
    }
}

export default Food