import Drawable from './Drawable'
import Shot from './Shot'

export default class Player extends Drawable {
    constructor(data, container){
        super(data, container)

        this.height = 30
        this.width = Math.round(this.container.canvas.width/(this.container.nb_shields*2+1))
        this.y = this.container.canvas.height - 100 - this.height
    }

    draw(){
        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x, this.y, this.width, this.height)
    }
}